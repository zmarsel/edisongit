﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using Edison.DAL;

namespace Edison.Server
{

    public class LobbyServer
    {
        private readonly static Lazy<LobbyServer> _instance = new Lazy<LobbyServer>(() => new LobbyServer());

        public LobbyServer()
        {

        }

        public static LobbyServer Instance => _instance.Value;
        public ObservableCollection<LobbyUser> Players { get; } = new ObservableCollection<LobbyUser>();

        public void AddPlayer(User user)
        {
            if (Players.Any(x => x.Id == user.Id))
            {
                return;
            }
            var player = new LobbyUser { Id = user.Id, Name = user.Name };
            Players.Add(player);
        }

        public void RemovePlayer(int userId)
        {
            if (Players.Any(x => x.Id == userId))
            {
                var player = Players.FirstOrDefault(x => x.Id == userId);
                Players.Remove(player);
            }
        }
    }
}