﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Edison.Server
{
    public enum LobbyState
    {
        [Display(Name= "LobbyState_WaitForPlayers", ResourceType=typeof(Lobby))]
        WaitForPlayers,
        [Display(Name = "LobbyState_Plaing", ResourceType = typeof(Lobby))]
        Plaing
    }
    public enum LobbyType
    {
        x2,
        x4
    }
    public class Lobby
    {
            public int Id { get; set; }
            public long timestamp { get; set; }
            public string Name { get; set; }
            public LobbyUser Owner { get; set; }
            public List<LobbyUser> Players { get; set; }
            public LobbyState State { get; set; }
            public LobbyType Type { get; set; }
    }
}