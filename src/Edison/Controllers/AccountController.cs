﻿using Edison.Helpers;
using Edison.Lang;
using Edison.Models;
using Edison.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Edison.DAL;
using Edison.Hub;
using Edison.Server;

namespace Edison.Controllers
{
    public class AccountController : BaseController
    {
        private readonly IUserService _userService;
        public AccountController(IUserService userService)
        {
            _userService = userService;
        }
        // GET: Account
        public ActionResult Login()
        {
            if (Request.IsAuthenticated)
            {
                LobbyServer.Instance.AddPlayer(_userService.GetUser(HttpContext.User.Identity.Name));
                return RedirectToAction("Index", "Players");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Login(LoginViewModel data)
        {
            if (ModelState.IsValid)
            {
                //var user = _userService.GetUser(data.Name);
                if (_userService.IsPasswordCorrect(data.Name, data.Password))
                {
                    LobbyServer.Instance.AddPlayer(_userService.GetUser(data.Name));
                    FormsAuthentication.SetAuthCookie(data.Name, data.RememberMe);
                    return RedirectToAction("Index", "Players");
                }
                else
                {
                    ModelState.AddModelError("", Account.Error_LoginDataIncorrect);
                }
            }
            return View(data);
        }
        [HttpPost]
        public ActionResult LogOff()
        {
            LobbyServer.Instance.RemovePlayer(_userService.GetUser(HttpContext.User.Identity.Name).Id);
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel data)
        {
            if (ModelState.IsValid)
            {
                bool result = _userService.RegisterUser(data);
                if (!result)
                {
                    ModelState.AddModelError("", Account.Error_RegistringUser);
                    return View(data);
                }
            }
            else
            {
                return View(data);
            }
            return Login(new LoginViewModel { Name = data.Name, Password = data.Password });
        }
        public ActionResult SetCulture(string culture)
        {
            // Validate input
            culture = CultureHelper.GetImplementedCulture(culture);
            // Save culture in a cookie
            HttpCookie cookie = Request.Cookies["_culture"];
            if (cookie != null)
                cookie.Value = culture;   // update cookie value
            else
            {
                cookie = new HttpCookie("_culture");
                cookie.Value = culture;
                cookie.Expires = DateTime.Now.AddYears(1);
            }
            Response.Cookies.Add(cookie);
            return RedirectToAction("Login");
        }
    }
}