﻿using Edison.DAL;
using Edison.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Edison.Models;
using static System.String;

namespace Edison.Service
{
    public interface IUserService
    {
        bool IsPasswordCorrect(string userName, string password);
        User GetUser(string login);
        bool RegisterUser(RegisterViewModel data);
    }
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepo;
        public UserService(IUserRepository userRepo)
        {
            _userRepo = userRepo;
        }

        public User GetUser(string login)
        {
            return _userRepo.FindBy(x => x.Name == login).FirstOrDefault();
        }

        public bool IsPasswordCorrect(string userName, string password)
        {
            var user = _userRepo.FindBy(x => x.Name == userName).FirstOrDefault();
            if (user == null) return false;
            return user.Password == password;
        }

        public bool RegisterUser(RegisterViewModel data)
        {
            if (_userRepo.FindBy(x => x.Name.ToLower() == data.Name.ToLower()).Any()) return false;
            _userRepo.Add(new User
            {
                Name = data.Name,
                Password = data.Password
            });
            _userRepo.Save();
            return true;
        }
    }
}