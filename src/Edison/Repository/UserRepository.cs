﻿using Edison.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Edison.Repository
{
    public interface IUserRepository : IGenericRepository<User>
    {
    }

    public class UserRepository : GenericRepository<EdisonContext, User>, IUserRepository
    {
    }
}