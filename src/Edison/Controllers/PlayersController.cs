﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Edison.Hub;
using Edison.Server;
using Edison.Service;

namespace Edison.Controllers
{
    public class PlayersController : BaseController
    {
        private readonly IUserService _userService;
        public PlayersController(IUserService userService)
        {
            _userService = userService;
        }

        public ActionResult Index()
        {
            return View(LobbyServer.Instance.Players.ToList());
        }

        public JsonResult GetOnlinePlayers()
        {
            return Json("", JsonRequestBehavior.AllowGet);
        }
    }
}