﻿using Edison.Lang;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Edison.Models
{
    public class RegisterViewModel
    {
        [Required]
        [StringLength(400)]
        [Display(Name = "Login", ResourceType = typeof(Account))]
        public string Name { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceName = "Error_Password", ErrorMessageResourceType = typeof(Account), MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password", ResourceType = typeof(Account))]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "ConfirmPassword", ResourceType = typeof(Account))]
        [Compare("Password", ErrorMessageResourceName = "Error_ComparePassword", ErrorMessageResourceType = typeof(Account))]
        public string ConfirmPassword { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Login", ResourceType=typeof(Account))]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password", ResourceType = typeof(Account))]
        public string Password { get; set; }

        [Display(Name = "RemeberMe", ResourceType = typeof(Account))]
        public bool RememberMe { get; set; }
    }
}