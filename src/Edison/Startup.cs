﻿using Microsoft.Owin;
using Owin;
[assembly: OwinStartup(typeof(Edison.Startup))]
namespace Edison
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}