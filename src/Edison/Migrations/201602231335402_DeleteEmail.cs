namespace Edison.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteEmail : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.User", "Email");
        }
        
        public override void Down()
        {
            AddColumn("dbo.User", "Email", c => c.String(maxLength: 400));
        }
    }
}
