﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Edison.DAL;
using Edison.Server;
using Microsoft.AspNet.SignalR;

namespace Edison.Hub
{
    public class LobbyHub : Microsoft.AspNet.SignalR.Hub
    {
        private readonly LobbyServer _server;

        public LobbyHub()
            : this(LobbyServer.Instance)
        {
        }
        LobbyHub(LobbyServer server)
        {
            _server = server;
            _server.Players.CollectionChanged += PlayersOnCollectionChanged;
        }

        private void PlayersOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            switch (notifyCollectionChangedEventArgs.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (LobbyUser player in notifyCollectionChangedEventArgs.NewItems)
                    {
                        LoginPlayer(player);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (LobbyUser player in notifyCollectionChangedEventArgs.OldItems)
                    {
                        LogoutPlayer(player.Id);
                    }
                    break;
                default:
                    break;
            }
        }

        public void LoginPlayer(LobbyUser player)
        {
            Clients.All.LoginPlayer(player);
        }

        public void LogoutPlayer(int userId)
        {
            Clients.All.LogoutPlayer(userId);
        }
    }
}