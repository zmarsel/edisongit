﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Edison.DAL
{
    public class EdisonContext : DbContext
    {
        public EdisonContext() : base("EdisonContext")
        {
        }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}